============================
paur: Yet another AUR helper
============================

Meta-meta
=========

What?
-----

**paur** is a free-software AUR_ helper written in Python 2.

The name—which is admittedly not very original or imaginative—is
pronounced similarly to the English word “power” (i.e., pou'ər).


Why?
----

 * Obviously not because there’s a shortage of `AUR helpers`_ in
   general, or even AUR helpers written in Python in particular.

 * Because I’m in between jobs at the moment, and the handful I’ve
   tried either had things they didn’t do, or they didn’t do those
   things the way I envisioned/expected/wanted/needed them to do it.

   i.e., I wanted to do it, and wanted to see if I could.


Design
======

**paur** is not, nor will it ever be, a wrapper of or a replacement for
**pacman**: Following the vein of Unix’s “tool” philosophy, **paur**
will always try to use existing applications where possible (i.e., I
don’t have to bend over backwards too much).

I also don’t plan on arbitrarily limiting myself to a maximum of *x*
lines of code, nor will I make exorbitant claims regarding how lean and
mean my code is.  My benchmark for **paur**’s “speed” will be on the
7-year old laptop I do all my work with.  Note that this laptop wasn’t
considered fast even when it first came out.

Any and all requests/complaints re: any of the above will be silently
ignored at best, or actively flamed if I’m feeling up to it.

That said, patches for things that are obviously broken are welcome.



.. _AUR: https://aur.archlinux.org/
.. _AUR helpers: https://wiki.archlinux.org/index.php/AUR_Helpers
