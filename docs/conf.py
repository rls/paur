import sys, os

sys.path.append(os.path.abspath('../'))

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
project = u'paur'
copyright = u'2013, Neil Santos'
version = '0.1'
release = '0.1.0-alpha'
exclude_trees = ['_build']
pygments_style = 'tango'

trim_footnote_reference_space = True

html_theme = 'pyramid'
# html_theme_path = './solar'
html_use_index = False
html_show_source = False
html_sidebars = {
    "**": ["localtoc.html"]
}
