#############################
paur — Yet another AUR helper
#############################

**paur** is a free-software AUR_ helper written in Python, and released under the GNU GPLv3_.  And while there’s surely no shortage of `AUR helpers`_ in general [#f1]_, **paur** tries to be unique in that’s it’s just a *little* bit more intelligent than the others of its ilk.  Its name—which, admittedly, is not very original nor imaginative—is pronounced similarly to the English word “power” (i.e., pou'ər).

.. _rationale:

*********
Rationale
*********

Most `AUR helpers`_ have very little, if any, differences in terms of actual operation and abilities: some may have a command interface partly inspired by CVS_, while some pattern their interfaces after `pacman`_’s.  However, all of them basically offer the following operations:

 * Download complete tarballs of one or more AUR recipes;
 * Build named AUR recipes; and,
 * Update/upgrade already installed AUR-based packages.

While this covers approximately 95% [#f2]_ of what people need from an AUR helper, I’ve at times needed them to do more.  And though I’ve tried quite a few of the selection, I’ve found that none of them had the features I wanted, or implemented them in a  *slightly off* way, enough that usage became an exercise in frustration.

Then, I remembered that I had this persistent delusion that I can *code*.

So, while it’ll be a trip, should other people find **paur** useful, bear in mind that it’s written primarily to satisfy *my* usage patterns; i.e., do not be surprised if it fails utterly to satisfy *you*.  (Although, if it frustrates you enough that you’re motivated into writing yet *another* AUR helper, that’d be **awesome**.  Unless, of course, it *sucks*.)

.. note:: Minor digression re: the design of **paur**

  **paur** is not—nor will it ever be—a wrapper of, or a replacement for **pacman**.  In the same vein as Unix’s “tool” philosophy, **paur** will always try to use existing applications where possible [#f3]_.  I also don’t plan on limiting myself to an arbitrary *x* lines of code, nor will I make exorbitant claims regarding how lean and mean my code is: my benchmark for **paur**’s speed will come from using it on the 7-year old laptop I do all my work with.  Note that this laptop wasn’t considered fast even when it first came out.

  Any and all requests/complaints re: any of the above will be silently ignored at best, or actively flamed if I’m feeling up to it.

  That said, patches for things that are *obviously* broken are welcome.


.. _features:

********
Features
********

Aside from the usual stuff (see :ref:`rationale`), **paur** hangs its hat on two features that are either things that have never (I think) been implemented properly, or haven’t been implemented at all: :ref:`dependency tracking <dependency-tracking>` and :ref:`recipe-update tracking <vcs-management>`.

.. _dependency-tracking:

Dependency Tracking
===================

I have, on my primary machine, quite handful of bleeding-edge packages installed; most, if not all, backed by an AUR recipe.  On more than one occasion, I’ve found a freshly built binary failing to run because it’s linked to a library that was built *after* it [#f4]_.  I’ve long considered it imperative that an AUR helper be aware of, and able to handle intelligently, this scenario; disappointingly, none of them (that I know of) currently do so.

To implement this to my satisfaction is **paur**’s primary *raison d'être*.  When updating AUR packages, it’ll update dependents *after* their dependencies.  It’ll also ask to rebuild affected packages if official packages it depends on get updated.  It won’t ever force the issue, unless you tell it to; after all, you presumably know what you’re doing.

But the hope is that you won’t ever be caught with a non-functioning binary again, simply because you couldn’t be bothered to remember that, *of course*, keeping an obscurely-named library that’s three levels within your favorite application’s dependency chain at a specific release number is *essential* to said application’s proper functioning`¡`_

.. note::

   At this point, **paur** can’t actually track dependencies of
   dependencies *[of dependencies…]* with any hope of accuracy (or
   staying sane), and this example is only for marketing purposes.
   Also, what **paur** *can* do will depend highly on how lazy the AUR
   recipe’s author(s) were in listing a package’s dependencies.

.. _vcs-management:

VCS-backed Recipe Management
============================

At the moment, there’s no easy way of tracking what changes between releases of a particular AUR recipe.  You can do it manually, for sure, except this, I feel, is something an AUR helper is perfectly positioned to do by itself.  Besides, you’d usually only ever want to check out changes *after* something breaks: tracking changes manually involves quite a bit of commitment for something you’d rarely use.

**paur** uses Mercurial internally to record changes between updates of an AUR recipe.  I chose it over git because it’s a *lot* simpler even for non-devs to figure out (and possibly a lot safer to play around with).  That, and—in my experience, at least—it eats up a lot less disk space for its history (which might come in handy if you’re planning on using **paur** for a significant amount of time).

.. note::

   This is an *optional* feature, and requires that `Mercurial`_ be installed and working.


.. _usage:

*****
Usage
*****

.. note:: Running **paur** for the first time

   When **paur** runs for the first time [#f5]_, it initializes a
   package-dependency graph, linking the non-repo packages it can find
   when its corresponding dependencies.  This could take a while,
   especially if you have quite a number of AUR packages installed.

Searching for, and retrieving info of, packages
===============================================

Downloading, building, and installing packages
==============================================


.. _configuration:

*************
Configuration
*************


.. _caveats:

*******
Caveats
*******


.. _contact:

*******
Contact
*******


.. _¡: https://en.wikipedia.org/wiki/Irony_mark#Temherte_slaq.C3.AE
.. _AUR: https://aur.archlinux.org/
.. _AUR helpers: https://wiki.archlinux.org/index.php/AUR_Helpers
.. _GPLv3: https://www.gnu.org/licenses/gpl.html
.. _CVS: https://savannah.nongnu.org/projects/cvs
.. _pacman: https://wiki.archlinux.org/index.php/pacman
.. _Mercurial: http://mercurial.selenic.com/

.. rubric:: Footnotes

.. [#f1] … or even AUR helpers written in Python in particular …
.. [#f2] Percentage from *exactly* zero research, and pulled *entirely* out of thin air.
.. [#f3] i.e., as long as I don’t have to bend over backwards *too* much.
.. [#f4] I’m looking at *you*, ImageMagick and Emacs.
.. [#f5] … or, rather, when it can’t read its package cache
