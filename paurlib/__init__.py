from __future__ import print_function
import errno, os.path, requests, subprocess, sys, tty, termios

try:
    if sys.stdout.isatty():
        from termcolor import colored, cprint
    else:
        raise
except:
    from .notermcolor import colored, cprint


__app_name__ = "paur"
__version__ = "0.1.0"
__description__ = "An intelligent AUR helper in Python"
__copyright__ = "Copyright 2013 Neil Santos"
__license__ = """This is free software with ABSOLUTELY NO WARRANTY.
For details, see https://www.gnu.org/licenses/gpl.html"""

class UnsafeExecutionError(OSError):
    def __init__(self, err_no=errno.EPERM, err_msg="Running as root; refusing to proceed"):
        super(UnsafeExecutionError, self).__init__(err_no, err_msg)

class PrivilegeDropFailedError(UnsafeExecutionError):
    def __init__(self):
        super(PrivilegeDropFailedError, self).__init__(err_msg="Unable to shed sudo-acquired privileges")


def reflow(max_len, text):
    if len(text) <= max_len:
        return [text]

    start = 0
    end = max_len - len(text)

    initial = text[start:end].strip()
    remainder = text[end:].strip()

    if len(initial) == max_len and remainder[0] != " ":
        # We've probably cut into the middle of a word.
        end = initial.rfind(" ")
        initial = text[start:end].strip()
        remainder = text[end:].strip()

    retval = [initial]
    retval.extend(reflow(max_len, remainder))
    return retval

def msg(message):
    prefix = ":: "
    prefix_len = len(prefix)
    rmessage = reflow(get_max_output_len(prefix_len), message)
    print(colored(prefix, "blue") + ("\n" + (" " * prefix_len)).join(rmessage))

def warn(message):
    prefix = "WARNING: "
    prefix_len = len(prefix)
    rmessage = reflow(get_max_output_len(prefix_len), message)
    cprint("{0}{1}".format(prefix, ("\n" + (" " * prefix_len)).join(rmessage)),
           "yellow", attrs=["bold"])

def error(message):
    prefix = "ERROR: "
    prefix_len = len(prefix)
    rmessage = reflow(get_max_output_len(prefix_len), message)
    cprint("{0}{1}".format(prefix, ("\n" + (" " * prefix_len)).join(rmessage)),
           "red", attrs=["bold"])

def get_window_dims():
    return map(int, subprocess.check_output(["stty", "size"]).split())

def get_max_output_len(adjust=0):
    _, cols = get_window_dims()
    return int((cols - adjust) * 0.85)

def getch(prompt=None):
    stdin_fd = sys.stdin.fileno()
    backup = termios.tcgetattr(stdin_fd)
    settings = backup[:]
    settings[3] &= ~termios.ICANON & ~termios.ECHO
    try:
        termios.tcsetattr(stdin_fd, termios.TCSADRAIN, settings)
        if prompt is not None:
            try:
                print(prompt, end="", flush=True) # flush= needed in Py3k
            except TypeError:
                print(prompt, end="")
        retval = sys.stdin.read(1)
        print("\r") # Next line, please
    finally:
        termios.tcsetattr(stdin_fd, termios.TCSADRAIN, backup)
    return retval

def confirm(msg, default=True):
    prompt_ques = ("[Y/n]" if default else "[y/N]")
    response = getch("{0} {1} ".format(msg, prompt_ques)).strip().lower()
    return (response in ("y", "") if default else response == "y")

def download(dest_path, url):
    req = requests.get(url)
    with open(os.path.abspath(dest_path),
              mode="wb", buffering=0) as tarball:
        tarball.write(req.content)

def download_t(dest_path, url):
    return (lambda: download(dest_path, url))
