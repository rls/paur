from __future__ import print_function
from .. import confirm, msg
from parched.aurpackage import AURPackage
import os, os.path


def setup_build_base_dir(config_parser):
    build_base_dir = config_parser.get("build_base_directory", is_path=True)
    if not os.path.isdir(build_base_dir):
        os.makedirs(build_base_dir, mode=0o755)
    return build_base_dir

def confirm_substitution(*subs):
    if subs:
        msg("The following keywords don't match an AUR package directly; possible substitutes found:")
        prompt = "  - Substitute '{0}' for '{1}'?"
        return dict([(key, pkg) for key, pkg in subs
                     if confirm(prompt.format(pkg["name"], key))])

def inform_substitution(*subs):
    if subs:
        msg("The following keywords don't match an AUR package directly; possible substitutes found, and will be used instead.")
        pattern = "  - Substituting '{0}' for '{1}'."
        [print(pattern.format(pkg["name"], key)) for key, pkg in subs]

def filter_packages(keywords, **kwargs):
    """Given a list of keywords, return feasible package names.

    Returns a list: the first item being a list that contains keywords
    that match an AUR package's name exactly; while the second item is a
    dict of one-and-only-one AUR package matches, keyed by
    keyword/package name.  Keywords that match no, or multiple, AUR
    package(s) are left out.
    """
    aur_ua = kwargs["aur_ua"]
    backing_store = kwargs["backing_store"]
    skip_confirmation = kwargs["skip_confirmation"]
    
    # Going to the wire is expensive, hence the store.
    direct_matches = dict([
        (keyword, match) for keyword, match in
        [(key, backing_store.get(key, direct_only=True)) for key in keywords]
        if match is not None
    ])
    # Do fuzzy-matching using the store.
    fuzzy_matches = dict([
        (keyword, match) for keyword, match in
        [(key, backing_store.get(key))
         for key in keywords
         if key not in direct_matches.keys()]
        if match is not None
    ])

    # Check for direct matches with AUR packages using remaining keywords.
    for_aur_match = [keyword for keyword in keywords
                     if keyword not in direct_matches.keys() and
                     keyword not in fuzzy_matches]
    if for_aur_match:
        # Go to the wire, looking for direct matches.
        aur_matches = aur_ua.info(*for_aur_match)
        match_names = [pkg["Name"] for pkg in aur_matches["results"]]
        direct_aur_matches = dict([
            (info["Name"], info)
            for info in aur_matches["results"]
            if info["Name"] in for_aur_match
        ])
        # Stash AUR info for future usage.
        for name, info in direct_aur_matches.items():
            backing_store.put_package(AURPackage(info))
            direct_matches[name] = info

    # Attempt to figure out suitable AUR packages for any
    # still-unmatched keywords.
    no_match_keywords = [keyword for keyword in keywords
                         if keyword not in direct_matches.keys() and
                         keyword not in fuzzy_matches]
    if no_match_keywords:
        aur_results = aur_ua.search(*no_match_keywords)
        fuzzy_aur_matches = dict([(keyword, aur_data["results"][0])
                                  for keyword, aur_data in aur_results.items()
                                  if aur_data["resultcount"] == 1])
        for name, info in fuzzy_aur_matches.items():
            backing_store.put_package(AURPackage(info))
            backing_store.alias(info["Name"], name)
            fuzzy_matches[name] = info["Name"]

    return [direct_matches, fuzzy_matches]
