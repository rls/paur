from . import filter_packages, search
from .. import aur, getch, msg, warn, error
from ..pacman import check_update_delta


def make_it_so(packages, **kwargs):
    check_update_delta(config_parser)
    msg("Retrieving pre-install meta for keyword(s): {0}".format(", ".join(packages)))

    targets = {}
    direct, vague = filter_packages(packages, backing_store, aur_ua, skip_confirmation)
    multi = [key for key in packages if key not in direct and key not in vague.keys()]

    if len(multi):
        warn("  ".join([
            "Ignoring the following, since they do not correspond to an AUR package.",
            "Re-run with more specific keywords, or use the search function."
        ]))
        print("  - {0}".format("\n  - ".join(multi)))

    # This ought not to be skippable.
    if len(vague):
        for keyword, pkg in vague.iteritems():
            confirm = getch("  - Substitute '{0}' for '{1}'? [Y/n] ".format(pkg["Name"], keyword))
            if confirm.lower() not in ["y", ""]:
                continue
