from . import confirm_substitution, inform_substitution, filter_packages, setup_build_base_dir
from .. import aur, confirm, download_t, msg, warn
from ..pacman import check_update_delta
import os, os.path, threading


def make_it_so(packages, **kwargs):
    msg("Retrieving pre-download meta for keyword(s): {0}".format(", ".join(packages)))

    targets, vague = filter_packages(packages, **kwargs)
    multi = [key for key in packages if key not in targets and key not in vague.keys()]

    if len(multi):
        warn("  ".join([
            "Ignoring the following, since they do not correspond to an AUR package.",
            "Re-run with more specific keywords, or use the search function."
        ]))
        print("  - {0}".format("\n  - ".join(multi)))

    if len(vague):
        confirmed = []
        if kwargs["skip_confirmation"]:
            inform_substitution(*vague.items())
            confirmed = vague
        else:
            confirmed = confirm_substitution(*vague.items())
        targets.update(confirmed)

    print("") # Skip a line, please
    msg("Tarballs for the following AUR packages will be downloaded: {0}".format(", ".join([target["name"] for target in targets.values()])))
    if not kwargs["skip_confirmation"] and not confirm("Proceed with download?"):
        return

    backing_store = kwargs["backing_store"]
    build_base_dir = setup_build_base_dir(kwargs["config_parser"])
    store_pkgs = [backing_store.get(pkg) for pkg in packages]
    for store_pkg in store_pkgs:
        target_url = store_pkg["urlpath"]
        target_path = os.path.join(build_base_dir, os.path.basename(target_url))
        threading.Thread(target=download_t(target_path, target_url)).start()
