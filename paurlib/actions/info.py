from .. import aur, get_window_dims, reflow
from prettytable import PrettyTable
import sys

try:
    if sys.stdout.isatty():
        from termcolor import colored, cprint
    else:
        raise
except:
    from ..notermcolor import colored, cprint


def make_it_so(results):
    rows, cols = get_window_dims()
    # Only ever use part of the available length for the description.
    max_desc_len = int(cols * 0.75)
    for pkg in results["results"]:
        pkg_info = PrettyTable([
            "",
            "{0} {1}".format(
                colored(pkg["Name"], "white", attrs=["bold"]),
                colored(pkg["Version"], "green")
            )],
            border=False
        )
        pkg_info.padding_width = 1
        pkg_info.align = "l"
        pkg_info.align[""] = "r"
        for attr, value in sorted(pkg.items()):
            if attr in ["ID", "Name", "URLPath", "URLPkgbuild", "Version"]:
                continue
            attr_name = attr
            if attr_name == "Description":
                value = "\n".join(reflow(max_desc_len, value))
            try:
                attr_name, post_process = aur.INFO_LOOKUP[attr]
                value = post_process(value)
            except KeyError:
                pass
            pkg_info.add_row((attr_name, value))
        print(pkg_info.get_string() + "\n")
