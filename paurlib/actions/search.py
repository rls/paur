from .. import get_window_dims, msg, reflow
import sys

try:
    if sys.stdout.isatty():
        from termcolor import colored, cprint
    else:
        raise
except:
    from paurlib.notermcolor import colored, cprint


def make_it_so(results):
    rows, cols = get_window_dims()
    # Only ever use part of the available length for the description.
    max_desc_len = int(cols * 0.85)
    
    for keyword, retdoc in sorted(results.items()):
        msg("Keyword: {0} - {1} match(es)".format(
            colored(keyword, "white", attrs=["bold", "underline"]),
            int(retdoc["resultcount"])
        ))

        for pkg in retdoc["results"]:
            quick_info = [
                colored(pkg["Name"], "white", attrs=["bold"]),
                colored(pkg["Version"], "green")
            ]
            if pkg["OutOfDate"]:
                quick_info.append(colored(
                    "[out-of-date]",
                    "red", 
                    attrs=["bold", "blink"]
                ))
            pkg_desc = (pkg["Description"].encode("utf-8")
                        if sys.version_info.major < 3 else
                        pkg["Description"])
            print("{0}\n  {1}".format(
                " ".join(quick_info),
                "\n  ".join(reflow(max_desc_len, pkg_desc))
            ))
    msg("Search summary: {0}".format(
        " / ".join(["{0} ({1})".format(
            colored(keyword, "white", attrs=["bold", "underline"]),
            colored(retdoc["resultcount"], "green")
        ) for keyword, retdoc in results.items()])
    ))
