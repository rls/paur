from .. import get_window_dims, msg, warn, error, reflow
import sys

try:
    if sys.stdout.isatty():
        from termcolor import colored, cprint
    else:
        raise
except:
    from paurlib.notermcolor import colored, cprint


def make_it_so(packages, **kwargs):
    rows, cols = get_window_dims()
    # Only ever use part of the available length for the description.
    max_desc_len = int(cols * 0.85)

    if len(packages) >= 1:
        msg(reflow(
            max_desc_len,
            "Trying to update package(s): {0}".format(", ".join(packages))
        ))
    elif len(packages) < 1:
        msg("Attempting to update all AUR packages...")

    # Check if a pacman update has been done recently; recommend one if
    # not.

    for package in packages:
        # Get all the info we have on the target package.

        # Figure out if any of the packages it depends on should be
        # updated, and in what order.

        # Build updated dependencies, if necessary.

        # Build updated binary and install.

        # Update package info.
        pass
