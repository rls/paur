from datetime import datetime
import json, paurlib

try:
    from urllib import quote_plus, urlencode
    from urlparse import urljoin, urlsplit, urlunsplit
except ImportError:
    from urllib.parse import quote_plus, urlencode, urljoin, urlsplit, urlunsplit

try:
    from urllib2 import build_opener
except ImportError:
    from urllib.request import build_opener

BASE_AUR_RPC_URL = "https://aur.archlinux.org/rpc.php"
SPLIT_AUR_RPC_URL = urlsplit(BASE_AUR_RPC_URL)

#: List of string representations for the integer category values in AUR
#: package data.
CATEGORY_LOOKUP = [
    "Invalid",
    "uncategorized",
    "daemons",
    "devel",
    "editors",
    "emulators",
    "games",
    "gnome",
    "i18n",
    "kde",
    "kernels",
    "lib",
    "modules",
    "multimedia",
    "network",
    "office",
    "science",
    "system",
    "x11",
    "xfce"
]

INFO_LOOKUP = {
    "CategoryID": ("Category", lambda x: CATEGORY_LOOKUP[x]),
    "FirstSubmitted": ("First submitted", lambda x: datetime.fromtimestamp(x).strftime("%c")),
    "LastModified": ("Last modified", lambda x: datetime.fromtimestamp(x).strftime("%c")),
    "NumVotes": ("Votes", lambda x: x),
    "OutOfDate": ("Out-of-date?", lambda x: "No" if (x == 0) else "Yes")
}


class Agent:
    # Note on response handling: At the moment, AUR doesn't specify an
    # encoding in its return documents, we take a chance and assume that
    # it's UTF-8.

    def __init__(self, timeout):
        self.timeout = timeout
        self.ua = build_opener()
        self.ua.addheaders = [
            ("User-agent", "{0} aur-ua/{1}".format(paurlib.__app_name__,
                                                   paurlib.__version__))
        ]

    def _add_pkg_urls(self, pkg):
        pkg["URLPath"] = urljoin(
            BASE_AUR_RPC_URL,
            pkg["URLPath"]
        )
        pkg["URLPkgbuild"] = urljoin(
            urljoin(
                BASE_AUR_RPC_URL,
                pkg["URLPath"]
            ),
            "PKGBUILD"
        )

    def search(self, *keywords):
        target_urls = {}
        # Build URLs before scraping in one go, so bandwidth isn't
        # wasted, in case something goes wrong.
        for keyword in keywords:
            rpc_url = list(SPLIT_AUR_RPC_URL)
            rpc_url[3] = urlencode({
                "type": "search",
                "arg": keyword
            })
            target_urls[keyword] = urlunsplit(rpc_url)

        retval = {}
        for keyword, url in target_urls.items():
            response = self.ua.open(url, timeout=self.timeout)
            retval[keyword] = json.loads(response.read().decode("utf-8"))

        for keyword, retdoc in retval.items():
            for pkg in retdoc["results"]:
                self._add_pkg_urls(pkg)
            retdoc["results"] = sorted(retdoc["results"],
                                       key=lambda x: x["Name"])
        return retval

    def info(self, *keywords):
        rpc_params = [("type", "multiinfo")]
        rpc_params += [("arg[]", quote_plus(x))
                       for x in keywords]

        rpc_url = list(SPLIT_AUR_RPC_URL)
        rpc_url[3] = urlencode(rpc_params)

        response = self.ua.open(urlunsplit(rpc_url), timeout=self.timeout)
        retval = json.loads(response.read().decode("utf-8"))
        retval["results"] = sorted(retval["results"],
                                   key=lambda x: x["Name"])
        for pkg in retval["results"]:
            self._add_pkg_urls(pkg)
        return retval
