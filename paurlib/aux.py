from . import aur, msg, warn, error
from .pacman import foreign_packages
from .store import BackingStore
from parched.localpackage import LocalPackage
import os, os.path, re, subprocess, sys

if sys.version_info.major > 2:
    raw_input = input


def populate_backing_store(config, store):
    aur_ua = aur.Agent(int(config["timeout"]))
    # Pull in data for each package
    pkgs = foreign_packages()
    msg("Parsing package data and caching pertinent meta...")

    aur_pkg_info = aur_ua.info(*[name for name, ver in pkgs])
    aur_pkgs = dict((aur_pkg["Name"], aur_pkg)
                    for aur_pkg in aur_pkg_info["results"])

    for pkgname, pkgver in pkgs:
        desc_path = os.path.join(
            config.get("pacman_db_root_path", is_path=True),
            "local",
            "{0}-{1}".format(pkgname, pkgver),
            "desc"
        )

        pkgbuild = None
        url_path = None

        if pkgname in aur_pkgs:
            pkgbuild = aur_pkgs[pkgname]["URLPkgbuild"]
            url_path = aur_pkgs[pkgname]["URLPath"]

        versplit = lambda x: tuple(re.split("([>=<]+)", x))
        pkg = LocalPackage(desc_path)
        store.put_package(pkg, pkgbuild, url_path)

def rebuild_backing_store(config, skip_confirmation=False):
    if not skip_confirmation:
        import readline

        warn("\n".join([
            "This will delete the current backing store (if it exists) and rebuild it from scratch.",
            "Building the backing store requires an Internet connection.",
            "It may take a while and should not be interrupted once it begins."
        ]))
        confirm = raw_input("\n".join([
            "\nIf you're sure you want to proceed, input \"YES\" (case-sensitive) to the following prompt:",
            "Proceed? ",
        ]))
        if confirm != "YES":
            return

    store_path = config.get("data_store_path", is_path=True)
    if os.path.isfile(store_path):
        msg("Deleting existing backing store...")
        os.remove(store_path)

    store = BackingStore(store_path)
    msg("Rebuilding backing store; this might take a while...")
    populate_backing_store(config, store)

    msg("Store built and populated.")
