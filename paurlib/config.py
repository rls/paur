try:
    from ConfigParser import ConfigParser
except ImportError:
    from configparser import ConfigParser
import os, os.path, paurlib

class Config:
    _def_section = paurlib.__app_name__
    _conf_parser = None

    def __init__(self):
        if os.getenv("XDG_CONFIG_HOME", None) is None:
            os.environ["XDG_CONFIG_HOME"] = "~/.config"
        xdg_config_home = os.path.expanduser(os.path.expandvars(
            os.getenv("XDG_CONFIG_HOME")
        ))
        with open(os.path.join(xdg_config_home, paurlib.__app_name__, "rc"), "rt") as cf_fd:
            self._conf_parser = ConfigParser(dict_type=dict)
            self._conf_parser.readfp(cf_fd)

    def __getitem__(self, key):
        return self.get(key)

    def get(self, key, is_path=False):
        retval = self._conf_parser.get(self._def_section, key)
        return (os.path.expanduser(os.path.expandvars(retval)))
