from __future__ import print_function

def colored(text, color=None, on_color=None, attrs=None):
    return text

def cprint(text, color=None, on_color=None, attrs=None, **kwargs):
    print(text, **kwargs)
