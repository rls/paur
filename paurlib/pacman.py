from . import error, msg, warn
from datetime import datetime
import mmap, os, re, subprocess

#: The string to search for, when trying to figure out when the last
#: time pacman tried a system upgrade.  This will need to be kept
#: up-to-date with pacman's releases.
PACMAN_LOG_UPDATE_MARKER = "starting full system upgrade"
#: Regular expression to match (and capture) a target log entry's
#: date/timestamp.  As with `PACMAN_LOG_UPDATE_MARKER`, will need to be
#: kept up-to-date with pacman's releases.
PACMAN_LOG_TIMESTAMP_RE = r"\[([-:0-9\s]+)\] \[PACMAN\]"


def foreign_packages():
    """Return a list of tuples with the name and version number of AUR packages.

    This pulls data directly from pacman, so could be a little slow,
    depending on certain circumstances (e.g., the mood of your computer,
    the phase of the moon, and/or the alignment of the planets)."""
    msg("Retrieving AUR-sourced packages...")
    return [raw_package.split()
            for raw_package in subprocess.check_output(
                    ["pacman", "-Qm"],
                    universal_newlines=True
            ).splitlines()]

def last_update_dtdelta(config_parser):
    """Return the timedelta between the current date/time and the last time
    pacman did a system update."""
    log_file_path = config_parser.get("pacman_log_file_path", is_path=True)
    retval = None
    with open(log_file_path, "r") as fd_log:
        try:
            mmap_log = mmap.mmap(fd_log.fileno(), 0, access=mmap.ACCESS_COPY)
            ridx = mmap_log.rfind(PACMAN_LOG_UPDATE_MARKER)
            last_nl = mmap_log.rfind("\n", 0, ridx)
            mmap_log.seek(last_nl + 1, os.SEEK_SET)

            # Extract log entry
            log_line = mmap_log.readline().strip()
            # Extract date/time stamp
            match = re.search(PACMAN_LOG_TIMESTAMP_RE, log_line)
            last_update = datetime.strptime(
                match.group(1),
                "%Y-%m-%d %H:%M"
            )
            retval = datetime.now() - last_update
        finally:
            mmap_log.close()
    return retval

def check_update_delta(config_parser):
    """Check delta for last system update, and warn or crap out as necessary."""
    allowed_delta = int(config_parser.get("allowed_update_delta"))
    days_since_update = last_update_dtdelta(config_parser).days

    if days_since_update > allowed_delta:
        error("It has been more than {0} day(s) since the last system update; you have to perform one before continuing.".format(allowed_delta))
        exit()
    elif days_since_update > 1 and days_since_update <= allowed_delta:
        warn("It has been {0} day(s) since the last system update; you should probably initiate one soon.".format(days_since_update))
