from .offtheshelf import openDB

class BackingStore:
    def __init__(self, store_path, online_only=False):
        self.store_path = store_path
        self.online_only = online_only
        self._store = openDB(store_path, protocol=2, use_lock=True)
        self._collection_pkg = self._store.get_collection("packages")

    def __len__(self):
        return len(self._store.collections)

    def cleanup(self):
        self._store.save()
        self._store.close()

    def alias(self, name, *aliases):
        pkg = self._collection_pkg.find_one(where={"name": name})
        if pkg:
            key = "aliases"
            cur_aliases = pkg.get(key, set())
            cur_aliases.update(aliases)
            self._collection_pkg.upsert(
                {key: set(cur_aliases)},
                where={"name": name}
            )

    def get(self, name, direct_only=False):
        if self.online_only:
            return None
        retval = self._collection_pkg.find_one(where={"name": name})
        if not direct_only and retval is None:
            retval = self._collection_pkg.find_one(where={"aliases": name})
        return retval

    def put(self, name, **details):
        if "name" not in details:
            details["name"] = name
        self._collection_pkg.upsert(details, where={"name": name})
        flag = self.online_only
        self.online_only = False
        retval = self.get(name, direct_only=True)
        self.online_only = flag
        return retval

    def put_package(self, pkg, pkgbuild_url=None, aur_url=None):
        # where pkg is an instance of parched.Package
        # --
        # pkgbuild_url= and aur_url= override values in pkg only if
        # their corresponding members don't exist, or are None.
        pkgbuild_url = getattr(pkg, "pkgbuild", pkgbuild_url)
        aur_url = getattr(pkg, "urlpath", aur_url)
        return self.put(pkg.name, **{
            "version": pkg.version,
            "release": pkg.release,
            "depends": [versplit(x) for x in pkg.depends],
            "makedepends": [versplit(x) for x in pkg.makedepends],
            "pkgbuild": pkgbuild_url,
            "urlpath": aur_url
        })

    def delete(self, name):
        return
