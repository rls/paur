from setuptools import setup
import sys

execfile("paurlib/__init__.py")

config = {
    "name": "paur",
    "version": __version__,
    "description": __description__,
    "author": "Neil Santos",
    "author_email": "python@neil.diosa.ph",
    "url": "http://rls.bitbucket.org/paur",
    "packages": ["paurlib"],
    "scripts": ["paur"],
    "classifiers": [
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Topic :: System :: Installation/Setup",
        "Topic :: Utilities"
    ],
    "requires": ["docopt (>= 0.6)", "termcolor (>= 1.1.0)", "PrettyTable (>= 0.6.1)"]
}

if sys.version_info.major == 3:
  # Py3k needs this to share shelve backends with 2.x
  config["requires"].append("bsddb3")
elif sys.version_info.major == 2:
  # Py2.x doesn't have this
  config["requires"].append("futures (>= 2.1.5)")

setup(**config)
